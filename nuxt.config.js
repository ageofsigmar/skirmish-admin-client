module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: 'y',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'skirmish admin theme' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },
  css: [
    '~assets/bootstrap/css/bootstrap.min.css',
    '~assets/font-awesome/css/font-awesome.min.css',
    '~assets/custom.css'
  ],
  js: [
    '~assets/bootstrap/js/bootstrap.min.js'
  ],
  /*
  ** Customize the progress-bar color
  */
  loading: {
    color: '#3B8070',
    height: '5px'
  },
  router: {
    middleware: 'check-auth'
  },
  /*
  ** Build configuration
  */
  modules: [
    '@nuxtjs/axios'
  ],
  plugins: ['~plugins/vue-notifications.js'],
  build: {
    vendor: ['vue-notifications', 'mini-toastr'],
    /*
    ** Run ESLINT on save
    */
    extend (config, ctx) {
      if (ctx.dev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  }
}
