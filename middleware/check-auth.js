import { getUserFromCookie, getUserFromLocalStorage } from '~/utils/auth'

export default function ({ isServer, store, redirect, req }) {
  if (isServer && !req) return
  const token = isServer ? getUserFromCookie(req) : getUserFromLocalStorage()
  if (token !== undefined && token !== null) {
    store.commit('SET_USER', token)
  } else {
    store.commit('SET_USER', null)
    return redirect('/')
  }
}
