import axios from 'axios'
import { getUserFromLocalStorage } from '~/utils/auth'

export const getFromApi = function (url) {
  return new Promise(function (resolve, reject) {
    if (axios.defaults.headers.common['x-access-token'] === undefined) {
      const token = getUserFromLocalStorage()
      axios.defaults.headers.common['x-access-token'] = token
    }
    axios.get('http://localhost:1337/' + url).then(function (res) {
      resolve(res)
    })
  })
}

export const removeFromApi = function (url, params) {
  return new Promise(function (resolve, reject) {
    if (axios.defaults.headers.common['x-access-token'] === undefined) {
      const token = getUserFromLocalStorage()
      axios.defaults.headers.common['x-access-token'] = token
    }
    axios.delete('http://localhost:1337/' + url, {params: params}).then(function (res) {
      resolve(res)
    })
  })
}

export const postToApi = function (url, params) {
  return new Promise(function (resolve, reject) {
    if (axios.defaults.headers.common['x-access-token'] === undefined) {
      const token = getUserFromLocalStorage()
      axios.defaults.headers.common['x-access-token'] = token
    }
    axios.post('http://localhost:1337/' + url, params).then(function (res) {
      resolve(res)
    })
  })
}

export const putToApi = function (url, params) {
  return new Promise(function (resolve, reject) {
    if (axios.defaults.headers.common['x-access-token'] === undefined) {
      const token = getUserFromLocalStorage()
      axios.defaults.headers.common['x-access-token'] = token
    }
    axios.put('http://localhost:1337/' + url, params).then(function (res) {
      resolve(res)
    })
  })
}
